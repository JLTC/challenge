import { Injectable }               from '@angular/core';
import { HttpClient, HttpHeaders }  from '@angular/common/http';
import { Employee }                 from '../shared/employee';
import { Observable, throwError }   from 'rxjs';
import { retry, catchError }        from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class RestApiService {
  
  // Define API
  apiURL = 'http://localhost:3000';
  apiURL2 = 'https://www.thecocktaildb.com/api/json/v1/1';
  

  constructor(private http: HttpClient) { }

  /*========================================
    List and Detail Methods for consuming RESTful API Challenge:
    https://www.thecocktaildb.com/api.php 
  =========================================*/

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }  

  // HttpClient API get() method => Fetch drinks list by "a" initial letter
  getDrinks(): Observable<Employee> {
    return this.http.get<Employee>(this.apiURL2 + '/search.php?f=a')
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }
  

  //List Categories
  getCategories(): Observable<Employee> {
    return this.http.get<Employee>(this.apiURL2 + '/list.php?c=list')
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }
  //Filter by Category
  getCategoriesFilter(value): Observable<Employee> {
    //console.log(value);
    return this.http.get<Employee>(this.apiURL2 + '/filter.php?c='+value)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }


 //List Glasses
  getGlasses(): Observable<Employee> {
    return this.http.get<Employee>(this.apiURL2 + '/list.php?g=list')
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }
  //Filter by Glasses
  getGlassesFilter(value): Observable<Employee> {
    //console.log(value);
    return this.http.get<Employee>(this.apiURL2 + '/filter.php?g='+value)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  //List Ingredients
  getIngredients(): Observable<Employee> {
    return this.http.get<Employee>(this.apiURL2 + '/list.php?i=list')
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }
  //Filter by Ingredients
  getIngredientsFilter(value): Observable<Employee> {
    return this.http.get<Employee>(this.apiURL2 + '/filter.php?i='+value)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }


  //List Alcoholic
  getAlcoholic(): Observable<Employee> {
    return this.http.get<Employee>(this.apiURL2 + '/list.php?a=list')
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }
  //Filter by kind of alcoholic
  getAlcoholicFilter(value): Observable<Employee> {
    return this.http.get<Employee>(this.apiURL2 + '/filter.php?a='+value)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }













  // HttpClient API get() method => Fetch employee
  getDrink(id): Observable<Employee> {
    return this.http.get<Employee>(this.apiURL2 + '/lookup.php?i=' + id)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }  

  // HttpClient API post() method => Create employee
  createEmployee(employee): Observable<Employee> {
    return this.http.post<Employee>(this.apiURL + '/employees', JSON.stringify(employee), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }  

  // HttpClient API put() method => Update employee
  updateEmployee(id, employee): Observable<Employee> {
    return this.http.put<Employee>(this.apiURL + '/employees/' + id, JSON.stringify(employee), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API delete() method => Delete employee
  deleteEmployee(id){
    return this.http.delete<Employee>(this.apiURL + '/employees/' + id, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // Error handling 
  handleError(error) {
     let errorMessage = '';
     if(error.error instanceof ErrorEvent) {
       // Get client-side error
       errorMessage = error.error.message;
     } else {
       // Get server-side error
       errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
     }
     window.alert(errorMessage);
     return throwError(errorMessage);
  }

}