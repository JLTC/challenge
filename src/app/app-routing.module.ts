import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DrinksListComponent } from './drinks-list/drinks-list.component';
import { DrinksDetailComponent } from './drinks-detail/drinks-detail.component';

const routes: Routes = [
  { path: 'drinks-list', component: DrinksListComponent },
  { path: 'drinks-detail', component: DrinksDetailComponent }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }