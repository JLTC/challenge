import { Component, OnInit } from '@angular/core';
import { RestApiService } from "../shared/rest-api.service";
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-drinks-detail',
  templateUrl: './drinks-detail.component.html',
  styleUrls: ['./drinks-detail.component.css']
})
export class DrinksDetailComponent implements OnInit {

  id = this.actRoute.snapshot.params['id'];
  drinkData: any = {};

  constructor(
    public restApi: RestApiService,
    public actRoute: ActivatedRoute,
    public router: Router
  ) { }

  ngOnInit() {

  this.loadAll()    
    
  }

  loadAll(){
    let userId = window.localStorage.getItem("idDrink");
    return this.restApi.getDrink(userId).subscribe((data: any) => {
      this.drinkData = data.drinks[0];      
    })
  }

}
