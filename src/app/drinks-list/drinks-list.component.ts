import { Component, OnInit } from '@angular/core';
import { RestApiService }    from "../shared/rest-api.service";
import { Router }            from "@angular/router";

@Component({
  selector: 'app-drinks-list',
  templateUrl: './drinks-list.component.html',
  styleUrls: ['./drinks-list.component.css']
})
export class DrinksListComponent implements OnInit {

  Drink:        any = [];
  categories:   any = [];
  glasses:      any = [];
  ingredients:  any = [];
  alcoholic:    any = [];

  constructor(
    public restApi: RestApiService,
    private router: Router
  ) { }


  ngOnInit() {
    this.loadDrinks()

    this.loadCategories()

    this.loadGlasses()

    this.loadIngredients()

    this.loadAlcoholic()
  }

  detailDrink(value: string): void {
    window.localStorage.removeItem("idDrink");
    window.localStorage.setItem("idDrink", value.toString());
    this.router.navigate(['/drinks-detail']);
  };

  // Get drinks list
  loadDrinks() {
    return this.restApi.getDrinks().subscribe((data: any) => {
      this.Drink = data.drinks;
    })
  }

  // Get Drinks Category
  loadCategories(){
    return this.restApi.getCategories().subscribe((data: any) => {
      this.categories = data.drinks;
    })
  }
  onSelectCategories(value:string){
    return this.restApi.getCategoriesFilter(value).subscribe((data: any) => {
      this.Drink = data.drinks;
    })
  }
  
  // Get Drinks Glasses
  loadGlasses(){
    return this.restApi.getGlasses().subscribe((data: any) => {
      this.glasses = data.drinks;
    })
  }
  onSelectGlasses(value:string){
    return this.restApi.getGlassesFilter(value).subscribe((data: any) => {
      this.Drink = data.drinks;
    })
  }

  // Get Drinks Ingredients
  loadIngredients(){
    return this.restApi.getIngredients().subscribe((data: any) => {
      this.ingredients = data.drinks;
    })
  }
  selectIngredients(value:string){
    return this.restApi.getIngredientsFilter(value).subscribe((data: any) => {
      this.Drink = data.drinks;
    })
  }
  
  // Get Drinks Alcoholic 
  loadAlcoholic(){
    return this.restApi.getAlcoholic().subscribe((data: any) => {
      this.alcoholic = data.drinks;
    })
  }
  onSelectAlcoholic(value:string){
    return this.restApi.getAlcoholicFilter(value).subscribe((data: any) => {
      this.Drink = data.drinks;
    })   
  }

}