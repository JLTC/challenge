import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrinksListComponent } from './drinks-list.component';

describe('DrinksListComponent', () => {
  let component: DrinksListComponent;
  let fixture: ComponentFixture<DrinksListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrinksListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrinksListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  //Karma Simple Testing

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create of Web-Drink',() => {
    const fixture = TestBed.createComponent(DrinksListComponent);
    const app     = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
  
  it('should have as title "angular-httpclient-app" ',() => {
    const fixture = TestBed.createComponent(DrinksListComponent);
    const app     = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('angular-httpclient-app');
  });


});
