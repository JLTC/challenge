# Angular 7 HttpClient Challenge for MichaelPage by JLTC

This project was built with Angular CLI version 7.2.3.

## To run project
Step 1
npm install

## Development server
Stpe 2
ng serve
Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Running unit Test with Karma
ng test

## Build create dist/ file to deploy and --prod
ng build 

